var app = {}

$(document).ready(function () {
    $('.ui-keyboard-button').click(function () {
        app.clickPadKey($(this));
    });
});

app.clickPadKey = function ($button) {
    var action = $button.attr('data-action');

    if (action === 'clear') {
        app.txt.val('');
    }
    if (action === 'delete') {
        app.txt.val(
            function (index, value) {
                return value.substr(0, value.length - 1);
            });
    }
    else {
        value = $button.attr('data-value');
        app.txt.val(app.txt.val() + value);
        app.txt.trigger('input')
    }

    $('.style-ok').prop('disabled', !app.validateValue());
}

app.validateValue = function () {
    if (app.txt.attr('maxLength') != undefined) {
        return app.txt.val().length >= app.txt.attr('maxLength');
    }
    else {
        return app.txt.val().length > 0;
    }
}
