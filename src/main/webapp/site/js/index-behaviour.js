$(document).ready(function () {
    $('#button-ok').click(function () {
        app.submitCard();
    });

    $('#button-exit').click(function () {
        app.resetPage();
    });

});


app.submitCard = function () {
    $.ajax({
        type: "POST",
        url: "blocked",
        data: {
            'id': $('#credit-card').val().replace(/\-/g, '')
        },
        context: document.body,
        dataType: 'json',
        success: function (data) {
            app.showPinPage(data);
        }
    });
}

app.authCard = function () {
    $('#message').css('display', 'none');
    $.ajax({
        type: "POST",
        url: "auth",
        data: {
            'id': $('#credit-card').val().replace(/\-/g, ''),
            'pin': $('#pin-card').val()
        },
        context: document.body,
        dataType: 'json',
        success: function (data) {
            if (data.blocked) {
                $(location).attr('href', 'blockmsg');
            }
            else if (data.success == null || !data.success) {
                app.errorAuth();
            }
            else {
                $(location).attr('href', 'card');
            }
        }
    });
}

app.showPinPage = function (data) {
    if (data.blocked == null || data.blocked) {
        $(location).attr('href', 'blockmsg');
        return;
    }
    $('.card').css('display', 'none');
    app.txt = $('#pin-card');
    $('.pin').css('display', 'block');
    $('#button-exit').css('display', 'block');
    var $buttonOk = $('#button-ok');
    $buttonOk.prop('disabled', true);
    $buttonOk.unbind().click(function () {
        app.authCard();
    });
}

app.resetPage = function () {
    app.txt.val('');
    $('.card').css('display', 'block');
    app.txt = $('#credit-card');
    app.txt.val('');
    $('.pin').css('display', 'none');
    $('#button-exit').css('display', 'none');
    var $buttonOk = $('#button-ok');
    $buttonOk.prop('disabled', true);
    $buttonOk.unbind().click(function () {
        app.submitCard();
    });
}

app.errorAuth = function () {
    $('#message').css('display', 'block');
    app.txt.val('');
    $('.style-ok').prop('disabled', !app.validateValue());
}
