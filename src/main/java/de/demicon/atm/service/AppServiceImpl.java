package de.demicon.atm.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.demicon.atm.dao.CardDao;
import de.demicon.atm.dao.OperationDao;
import de.demicon.atm.domain.Card;
import de.demicon.atm.domain.Operation;

import static de.demicon.atm.domain.Operation.CODE_REMOVE;

/**
 */
@Service ("appService")
@Transactional
public class AppServiceImpl implements AppService
{
    private AuthenticationDetailsSource<HttpServletRequest, ?> authenticationDetailsSource = new WebAuthenticationDetailsSource();

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CardDao cardDao;

    @Autowired
    private OperationDao operationDao;

    @Override
    public Card findCard(String id)
    {
        return cardDao.findById(id);
    }

    @Override
    public Boolean blockedCard(String id)
    {
        Card card = cardDao.findById(id);
        return card != null ? card.isBlocked() : null;
    }

    @Override
    public void blockCard(String id)
    {
        Card card = cardDao.findById(id);
        if (card != null)
        {
            card.setBlocked(true);
            cardDao.updateCard(card);
        }
    }

    @Override
    public Authentication authCardHolder(String id, String pin, HttpServletRequest request)
    {
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(id, pin);
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));

        return authenticationManager.authenticate(authRequest);
    }

    @Override
    @PreAuthorize ("hasRole('ROLE_CARDHOLDER')")
    public boolean removeSum(String id, Integer sum)
    {
        Card card = cardDao.findById(id);
        int newBalance = card.getBalance() - sum;
        if (newBalance >= 0)
        {
            card.setBalance(newBalance);
            cardDao.updateCard(card);

            Operation operation = new Operation(id, new Date(), CODE_REMOVE, sum, newBalance);
            operationDao.saveCard(operation);

            return true;
        }
        return false;
    }

    @Override
    @PreAuthorize ("hasRole('ROLE_CARDHOLDER')")
    public List<Operation> getAllOperation(String cardId)
    {
        return operationDao.findByCardId(cardId);
    }
}
