package de.demicon.atm.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;

import de.demicon.atm.domain.Card;
import de.demicon.atm.domain.Operation;

/**
 */
public interface AppService
{
    Card findCard(String id);

    Boolean blockedCard(String id);

    Authentication authCardHolder(String id, String pin, HttpServletRequest request);

    boolean removeSum(String id, Integer sum);

    List<Operation> getAllOperation(String cardId);

    void blockCard(String id);
}
