package de.demicon.atm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import de.demicon.atm.domain.Card;

/**
 */
@Service ("securityService")
public class SecurityService implements UserDetailsService
{
    private static final Logger LOG = LoggerFactory.getLogger(SecurityService.class);

    public static final SimpleGrantedAuthority ROLE_CARDHOLDER = new SimpleGrantedAuthority("ROLE_CARDHOLDER");

    @Autowired
    private AppService appService;

    @Override
    public UserDetails loadUserByUsername(String cardId) throws UsernameNotFoundException
    {

        LOG.debug("loadUserByUsername. {}", cardId);

        Card card = appService.findCard(cardId);

        if (card == null || card.isBlocked())
        {
            throw new UsernameNotFoundException("Card with id not found " + cardId);
        }

        return new User(card.getId(), card.getPin(), Lists.<GrantedAuthority>newArrayList(ROLE_CARDHOLDER));
    }
}
