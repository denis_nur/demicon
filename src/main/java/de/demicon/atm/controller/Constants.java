package de.demicon.atm.controller;

/**
 */
public interface Constants
{
    String MSG_INVALID_CARD_NUMBER_OR_YOUR_CARD_IS_BLOCKED = "Invalid card number or your card is blocked.";
    String MSG_NOT_ENOUGH_BALANCE = "Sorry, your balance is not enough for operation.";

}
