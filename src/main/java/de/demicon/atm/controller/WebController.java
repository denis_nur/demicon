package de.demicon.atm.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import de.demicon.atm.service.AppService;

/**
 */
@Controller
@RequestMapping ("/")
@SessionAttributes ("AUTH_ATTEMPT_KEY")
public class WebController
{
    private static final Logger LOG = LoggerFactory.getLogger(WebController.class);

    public static final int MAX_AUTH_ATTEMPT = 3;

    @Autowired
    private AppService service;

    @RequestMapping (value = {"/", "/index"})
    public String index(final Model model)
    {
        final SoyModel soyModel = new SoyModel();
        model.addAttribute("model", soyModel);
        return "soy:de.demicon.atm.index";
    }

    @RequestMapping (value = "/blockmsg")
    public String blockMsg(final Model model)
    {
        final SoyModel soyModel = new SoyModel();
        soyModel.setValue(Constants.MSG_INVALID_CARD_NUMBER_OR_YOUR_CARD_IS_BLOCKED);
        soyModel.setBackUrl("index");
        model.addAttribute("model", soyModel);
        return "soy:de.demicon.atm.msg";
    }

    @RequestMapping (value = "/blocked", method = RequestMethod.POST)
    @ResponseBody
    public String cardIsBlocked(@RequestParam ("id") String id) throws IOException
    {
        Boolean blocked = service.blockedCard(id);
        return "{\"blocked\":" + blocked + "}";
    }

    @RequestMapping (value = "/auth", method = RequestMethod.POST)
    @ResponseBody
    public String cardIsBlocked(@RequestParam ("id") String id, @RequestParam ("pin") String pin, @ModelAttribute ("AUTH_ATTEMPT_KEY") Integer attempt, Model model, SessionStatus sessionStatus, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        try
        {
            Authentication authentication = service.authCardHolder(id, pin, request);
            successfulAuthentication(request, response, authentication);
        }
        catch (AuthenticationException e)
        {
            if (attempt > MAX_AUTH_ATTEMPT)
            {
                service.blockCard(id);
                sessionStatus.setComplete();
                return "{\"blocked\":" + true + "}";
            }
            model.addAttribute("AUTH_ATTEMPT_KEY", attempt + 1);

            return "{\"success\":" + false + "}";
        }
        catch (IOException e)
        {
            throw new ServletException(e);
        }
        return "{\"success\":" + true + "}";
    }

    private void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException
    {
        LOG.debug("Authentication success: {}", authResult.toString());
        SecurityContextHolder.getContext().setAuthentication(authResult);
    }

    @ModelAttribute ("AUTH_ATTEMPT_KEY")
    public Integer initialAttempt()
    {
        return 1;
    }
}
