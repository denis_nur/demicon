package de.demicon.atm.controller.dto;

/**
 */
public class Operation
{
    private String cardId;

    private String create;

    private int code;

    private int amount;

    private int balance;

    public Operation(String cardId, String create, int code, int amount, int balance)
    {
        this.cardId = cardId;
        this.create = create;
        this.code = code;
        this.amount = amount;
        this.balance = balance;
    }

    public String getCardId()
    {
        return cardId;
    }

    public void setCardId(String cardId)
    {
        this.cardId = cardId;
    }

    public String getCreate()
    {
        return create;
    }

    public void setCreate(String create)
    {
        this.create = create;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    public int getBalance()
    {
        return balance;
    }

    public void setBalance(int balance)
    {
        this.balance = balance;
    }
}
