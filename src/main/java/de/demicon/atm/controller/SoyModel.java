package de.demicon.atm.controller;

import java.util.List;

import de.demicon.atm.controller.dto.Operation;

/**
 */
public class SoyModel
{
    private String siteUrl = "/atm-sim";

    private String value;

    private int balance;

    private String time;

    private String id;

    private String backUrl;

    private List<Operation> operations;

    public String getSiteUrl()
    {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl)
    {
        this.siteUrl = siteUrl;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getTime()
    {
        return time;
    }

    public int getBalance()
    {
        return balance;
    }

    public void setBalance(int balance)
    {
        this.balance = balance;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setBackUrl(String backUrl)
    {
        this.backUrl = backUrl;
    }

    public String getBackUrl()
    {
        return backUrl;
    }

    public void setOperations(List<Operation> operations)
    {
        this.operations = operations;
    }

    public List<Operation> getOperations()
    {
        return operations;
    }
}
