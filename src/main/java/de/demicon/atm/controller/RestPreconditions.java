package de.demicon.atm.controller;

/**
 */
public class RestPreconditions
{
    public static <T> T checkFound(T resource)
    {
        if (resource == null)
        {
            throw new RuntimeException();
        }
        return resource;
    }
}