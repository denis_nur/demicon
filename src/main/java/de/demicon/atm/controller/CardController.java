package de.demicon.atm.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.demicon.atm.domain.Card;
import de.demicon.atm.domain.Operation;
import de.demicon.atm.service.AppService;

/**
 * Secured Card Controller
 */
@Controller
public class CardController
{
    @Autowired
    private AppService service;

    @RequestMapping (value = "/card", method = RequestMethod.GET)
    public String operations(final Model model)
    {
        final SoyModel soyModel = new SoyModel();
        model.addAttribute("model", soyModel);
        return "soy:de.demicon.atm.operations";
    }

    @RequestMapping (value = "/card/balance", method = RequestMethod.GET)
    public String balance(Model model)
    {
        Card card = service.findCard(getCurrentUserId());

        final SoyModel soyModel = new SoyModel();
        soyModel.setTime(serverTime());
        soyModel.setBalance(card.getBalance());
        soyModel.setId(card.getId());
        model.addAttribute("model", soyModel);
        return "soy:de.demicon.atm.balance";
    }

    @RequestMapping (value = "/card/withdrawals", method = RequestMethod.GET)
    public String withdrawals(final Model model)
    {
        final SoyModel soyModel = new SoyModel();
        model.addAttribute("model", soyModel);
        return "soy:de.demicon.atm.withdrawals";
    }

    @RequestMapping (value = "/card/remove/{sum}", method = RequestMethod.GET)
    public String withdrawals(@PathVariable Integer sum, Model model)
    {
        String cardId = getCurrentUserId();
        boolean result = service.removeSum(cardId, sum);
        final SoyModel soyModel = new SoyModel();
        model.addAttribute("model", soyModel);
        if (result)
        {
            List<Operation> operations = service.getAllOperation(cardId);
            soyModel.setOperations(convertOperations(operations));
            return "soy:de.demicon.atm.report";
        }
        else
        {
            soyModel.setValue(Constants.MSG_NOT_ENOUGH_BALANCE);
            soyModel.setBackUrl("card/withdrawals");
            return "soy:de.demicon.atm.msg";
        }
    }

    private List<de.demicon.atm.controller.dto.Operation> convertOperations(List<Operation> values)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");
        List<de.demicon.atm.controller.dto.Operation> res = new ArrayList<>();
        for (Operation op : values)
        {
            res.add(new de.demicon.atm.controller.dto.Operation(op.getCardId(), formatter.format(op.getCreate()), op.getCode(), op.getAmount(), op.getBalance()));
        }
        return res;
    }

    private String serverTime()
    {
        return new SimpleDateFormat("dd.MM.YYYY").format(new Date());
    }

    public String getCurrentUserId()
    {
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (obj != null)
        {
            UserDetails userDetails = (UserDetails) obj;
            return userDetails.getUsername();
        }
        else
        {
            return null;
        }
    }
}
