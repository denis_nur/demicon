package de.demicon.atm;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import de.demicon.atm.config.AppConfig;
import de.demicon.atm.config.DataConfig;
import de.demicon.atm.config.SecurityConfig;

public class WebAppInit extends AbstractAnnotationConfigDispatcherServletInitializer
{
    @Override
    protected Class<?>[] getRootConfigClasses()
    {
        return new Class[]{SecurityConfig.class, AppConfig.class, DataConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses()
    {
        return new Class[]{SecurityConfig.class, AppConfig.class, DataConfig.class};
    }

    @Override
    protected String[] getServletMappings()
    {
        return new String[]{"/"};
    }
}
