package de.demicon.atm.dao;

import de.demicon.atm.domain.Card;

/**
 */
public interface CardDao
{
    void saveCard(Card card);

    Card findById(String id);

    void updateCard(Card card);
}
