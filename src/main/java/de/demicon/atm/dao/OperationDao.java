package de.demicon.atm.dao;

import java.util.List;

import de.demicon.atm.domain.Operation;

/**
 */
public interface OperationDao
{
    void saveCard(Operation operation);

    List<Operation> findByCardId(String cardId);
}
