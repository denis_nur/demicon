package de.demicon.atm.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import de.demicon.atm.domain.Card;

/**
 */
@Repository ("cardDao")
public class CardDaoImpl extends AbstractDao implements CardDao
{
    @Override
    public void saveCard(Card card)
    {
        persist(card);
    }

    @Override
    public Card findById(String id)
    {
        Criteria criteria = getSession().createCriteria(Card.class);
        criteria.add(Restrictions.eq("id", id));
        return (Card) criteria.uniqueResult();
    }

    @Override
    public void updateCard(Card card)
    {
        getSession().update(card);
    }
}
