package de.demicon.atm.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import de.demicon.atm.domain.Operation;

/**
 */
@Repository ("operationDao")
public class OperationDaoImpl extends AbstractDao implements OperationDao
{
    @Override
    public void saveCard(Operation operation)
    {
        persist(operation);
    }

    @Override
    public List<Operation> findByCardId(String cardId)
    {
        Criteria criteria = getSession().createCriteria(Operation.class);
        criteria.add(Restrictions.eq("cardId", cardId));
        criteria.addOrder(Order.asc("create"));
        return criteria.list();
    }
}
