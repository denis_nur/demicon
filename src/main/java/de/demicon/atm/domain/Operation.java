package de.demicon.atm.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 */
@Entity
@Table (name = "Operation")
public class Operation
{
    public static final int CODE_REMOVE = 100;

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long id;

    @Column (name = "card_id")
    private String cardId;

    private Date create;

    private int code;

    private int amount;

    private int balance;

    public Operation()
    {
    }

    public Operation(String cardId, Date create, int code, int amount, int balance)
    {
        this.cardId = cardId;
        this.create = create;
        this.code = code;
        this.amount = amount;
        this.balance = balance;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCardId()
    {
        return cardId;
    }

    public void setCardId(String cardId)
    {
        this.cardId = cardId;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    public int getBalance()
    {
        return balance;
    }

    public void setBalance(int balance)
    {
        this.balance = balance;
    }

    public Date getCreate()
    {
        return create;
    }

    public void setCreate(Date create)
    {
        this.create = create;
    }
}
