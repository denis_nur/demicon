package de.demicon.atm.service;

import de.demicon.atm.config.AppConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpSession;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class}, loader = AnnotationConfigWebContextLoader.class)
@WebAppConfiguration
public class CardSecuredServiceTest {

    private String[] testIDNormal = {"0000000000000001", "0001"};

    @Autowired
    private AppService appService;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void getCardServiceUnauthenticated() {
        appService.removeSum(testIDNormal[0], 100);
    }

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testIndex() throws Exception {
        mockMvc.perform(post("/")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=utf-8"));
    }

    @Test
    public void testProvideBlockCard() throws Exception {
        String cardId = testIDNormal[0];

        mockMvc.perform(post("/blocked")
                .param("id", cardId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.blocked").value(false));
    }

    @Test
    public void testBlockedCard() throws Exception {
        String cardId = testIDNormal[0];

        HttpSession session = mockMvc.perform(post("/blocked")
                .param("id", cardId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.blocked").value(false))
                .andReturn()
                .getRequest()
                .getSession();

        Assert.assertNotNull(session);

        for (int i = 0; i < 3; i++) {
            mockMvc.perform(post("/auth")
                    .session((MockHttpSession) session)
                    .param("id", cardId)
                    .param("pin", "1111")
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(jsonPath("$.success").value(false));

        }

        mockMvc.perform(post("/auth")
                .session((MockHttpSession) session)
                .param("id", cardId)
                .param("pin", "1111")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.blocked").value(true));


        mockMvc.perform(post("/blocked")
                .session((MockHttpSession) session)
                .param("id", cardId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.blocked").value(true));
    }

    @Test
    public void testAuth() throws Exception {
        authenticate(testIDNormal[0], testIDNormal[1])
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.success").value(true));
    }

    private ResultActions authenticate(String id, String pin) throws Exception {
        ResultActions perform = mockMvc.perform(post("/auth")
                .param("id", id)
                .param("pin", pin)
                .accept(MediaType.APPLICATION_JSON));
        return perform;
    }

    @Test
    public void testInvalidAuth() throws Exception {
        mockMvc.perform(post("/auth")
                .param("id", testIDNormal[0])
                .param("pin", testIDNormal[0])
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.success").value(false));
    }

    //@Test
    public void testOperationsNotAuth() throws Exception {
        mockMvc.perform(get("/card")
                .accept(MediaType.TEXT_HTML))
                .andExpect(redirectedUrl("/"));
    }

    //@Test(expected = Exception.class)
    //@WithUserDetails("0000000000000011")
    public void testOperationsBlock() throws Exception {
        mockMvc.perform(get("/card")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @WithUserDetails("0000000000000001")
    public void testOperations() throws Exception {
        mockMvc.perform(get("/card")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=utf-8"));
    }


}
